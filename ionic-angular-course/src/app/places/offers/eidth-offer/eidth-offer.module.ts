import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EidthOfferPageRoutingModule } from './eidth-offer-routing.module';

import { EidthOfferPage } from './eidth-offer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EidthOfferPageRoutingModule
  ],
  declarations: [EidthOfferPage]
})
export class EidthOfferPageModule {}
