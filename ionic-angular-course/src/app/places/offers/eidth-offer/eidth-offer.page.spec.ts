import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EidthOfferPage } from './eidth-offer.page';

describe('EidthOfferPage', () => {
  let component: EidthOfferPage;
  let fixture: ComponentFixture<EidthOfferPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EidthOfferPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EidthOfferPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
