import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EidthOfferPage } from './eidth-offer.page';

const routes: Routes = [
  {
    path: '',
    component: EidthOfferPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EidthOfferPageRoutingModule {}
